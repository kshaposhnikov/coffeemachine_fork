#include <iostream>
#include <ctime>
#include "coffeemachine.h"

using namespace std;

int main() {
    srand(time(NULL));
    int level = rand() % 1000;
    cout << "Level of water: " << level << endl;
    int option = 0;
    Tea t;
    Coffee c;
    cout << "Enter 1 - for coffee, 2 - for tea: " << endl;
    cin >> option;
    if(option == 1) {
        if(c.Start(level)) {
            Temperature(c);
        }
    }
    else if(option == 2) {
        if(t.Start(level)) {
            Temperature(t);
        }
    }
    return 0;
}
