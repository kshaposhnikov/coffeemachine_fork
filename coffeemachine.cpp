#include "coffeemachine.h"

using namespace std;

bool CoffeeMachine::Start(int LevelWater) {
    if(CheckWater(LevelWater)) {
        handleBill();
        cout << "Please, wait about 1 minute, while drink will be ready." << endl;
        return true;
    }
    else {
        cout << "Sorry, low level of water! Add more water." << endl;
        return false;
    }
}

void CoffeeMachine::Temperature() const {
    cout << "Temperature of Your drink 100 degrees" << endl;
}

std::string CoffeeMachine::FormattedPrice() {
    return std::to_string(getPriceValue()) + "$";
}


float CoffeeMachine::getPriceValue() const {
    return 0;
}

bool CoffeeMachine::CheckWater(int LevelWater) {
    if(LevelWater > 100) {
        return true;
    }
    else {
        return false;
    }
}

void CoffeeMachine::handleBill() {
    cout << "Price of Your drink " + FormattedPrice() << endl;
    cout << "Please deposit money: ";
    float money;
    cin >> money;

    if (money < getPriceValue()) {
        throw runtime_error("Please deposit at least " + FormattedPrice() + ". Your money has been returned.");
    }

    cout << "Your short-change is " << std::to_string(money - getPriceValue()) << endl;
}

void Tea::Temperature() const {
    cout << "Temperature of Your drink 80 degrees" << endl;
}


float Tea::getPriceValue() const {
    return 1;
}

void Coffee::Temperature() const {
    cout << "Temperature of Your drink 70 degrees" << endl;
}

float Coffee::getPriceValue() const {
    return 1.5;
}

void Temperature(const CoffeeMachine &obj) {
    obj.Temperature();
}
