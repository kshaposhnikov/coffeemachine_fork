#ifndef COFFEEMACHINE_H
#define COFFEEMACHINE_H

#include<iostream>

class CoffeeMachine {
public:
    bool Start(int LevelWater);
    virtual void Temperature() const;
    std::string FormattedPrice();

protected:
    virtual float getPriceValue() const;

private:
    bool CheckWater(int LevelWater);
    void handleBill();
};

class Tea : public CoffeeMachine {
public:
    void Temperature() const;

protected:
    float getPriceValue() const;
};

class Coffee : public CoffeeMachine {
public:
    void Temperature() const;

protected:
    float getPriceValue() const;
};

void Temperature(const CoffeeMachine &obj);

#endif // COFFEEMACHINE_H
